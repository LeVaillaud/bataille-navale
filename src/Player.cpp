#include "Player.h"

Player::Player()
{
    this->setShips();

}

Player::~Player()
{
    //dtor
}

void Player::setShips()
{
    for(int i=0; i<4; i++)
    {
       this->m_ships.push_back(SousMarin());
    }

    for(int i=0; i<3; i++)
    {
       this->m_ships.push_back(Destroyer());
    }

    for(int i=0; i<2; i++)
    {
       this->m_ships.push_back(Croiseur());
    }

    this->m_ships.push_back(Cuirasse());
    this->placeShips();
}

void Player::placeShips()
{
     srand(time(NULL));
    int position=0; //variable qui permet de tenter le placement de bateau dans les quatre direction. Prendra donc valeur de 0 � 3.
    int x, y, xtemp, ytemp;
    bool placement= true;
    bool placementTemp;



    for(int i=0; i<this->m_ships.size(); i++) //placement de tous les navires
    {
        //std::cout<<"bateau"<<i<<std::endl;
        placement=false; //le placement part impossible
        while(!placement) //tant que le placement n'est pas possible
        {
            //std::cout<<"bateau"<<i<<std::endl;
            placementTemp=true; //placement temporaire impossible
            position=0;
            x=rand()%15;
            y=rand()%15;
            xtemp=x;
            ytemp=y;
            //std::cout<<x<<std::endl;
            while(position!=4){
                placementTemp=true;
                for(int j=0; j<this->m_ships[i].m_shipParts.size(); j++){ //pour toutes les parties du bateau � placer
                    if(xtemp<=14 && xtemp>=0 && ytemp<=14 && ytemp>=0){ // si la case est dans le tableau
                        for(int k=0; k<this->m_ships.size(); k++){
                            for(int l=0; l<this->m_ships[k].getLength(); l++){ //pour toutes les cases de tous les bateaux
                                //std::cout<<"bateau"<<i<<std::endl;
                                //if(this->m_ships[k].m_shipParts[l].getPosition()[0]==xtemp || this->m_ships[k].m_shipParts[l].getPosition()[1]==ytemp) placementTemp=false; //si une cpartie d'un autre bateau est � cet endroit, position non-praticable
                                //std::cout<<"bateau"<<i<<std::endl;
                            }
                           // std::cout<<"bateau"<<i<<std::endl;
                        }
                       // std::cout<<"bateau"<<i<<std::endl;
                        switch(position)
                        {
                            case 0: xtemp++;
                                    break;
                            case 1: ytemp++;
                                    break;
                            case 2: xtemp--;
                                    break;
                            case 3: ytemp--;
                                    break;
                        }


                    }
                    else placementTemp=false;
                }
                //std::cout<<"bateau"<<i<<std::endl;
                if(placementTemp==true){ //si le placement temporaire est possible, alors le placement est possible
                    placement=true;
                    break;
                }
                else position++; //si le placement est correct, on sort de la boucle, sinon on tente une autre orientation
            }
        }

        for(int j=0; j<this->m_ships[i].m_shipParts.size(); j++){

            this->m_ships[i].m_shipParts[j].setPosition(x,y);
            switch(position)
            {
                case 0: x++;
                        break;
                case 1: y++;
                        break;
                case 2: x--;
                        break;
                case 3: y--;
                        break;
            }

        }



    }

}


