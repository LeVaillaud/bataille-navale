#include "Game.h"
#include <iostream>
#include <windows.h>
#include <conio.h>
Game::Game()
    :m_win(0),m_turn(1), m_p1(Player()), m_p2(Player())
{
    //ctor
    std::cout<<"coucou"<<std::endl;
}

Game::~Game()
{
    //dtor
}
///La fonction affiche() affiche simplement les deux grilles de jeu � l'�cran
void Game::affiche()
{
    //std::cout<<"coucou";
    this->gotoligcol(1,0);
    for(int i=0;i<16;i++)
    {
        for(int j=0;j<16;j++)
        {
            std::cout<<"  |"; //on s'occupe des "cellules" du tableau 1
        }

        std::cout<<"          ";
         for(int j=0;j<16;j++)
        {
            std::cout<<"  |"; //cellules du tableau 2
        }

        std::cout<<std::endl;
        for(int j=0;j<16;j++)
        {
            std::cout<<"--|"; //lignes de s�paration des cellules du tableau 1
        }
        std::cout<<"          ";
        for(int j=0;j<16;j++)
        {
            std::cout<<"--|"; //lignes de s�paration des cellules du tableau 1
        }
        std::cout<<std::endl;
    }

    //On affiche les num�ros de colonnes des tableaux 1 et 2
    for(int i=0;i<15;i++)
    {
        gotoligcol(1,3+i*3);
        std::cout<<i;
        gotoligcol(1,61+i*3);
        std::cout<<i;
    }
    //On affiche les noms des lignes des deux tableaux
    for(int i=0;i<15;i++)
    {
        gotoligcol(2*i+3,1);
        std::cout<<(char)(97+i);
        gotoligcol(2*i+3,59);
        std::cout<<(char)(97+i);
    }
    this->afficheJoueur();
    this->afficheOpponent();
    this->gotoligcol(35,30);
    std::cout<<"Joueur: "<<this->getTurn();
}

void Game::afficheJoueur()
{
    if(this->getTurn()==1)
    {
        for(unsigned int i=0; i<this->m_p1.m_ships.size();i++)
        {
            for(int j=0; j<this->m_p1.m_ships[i].getLength(); j++)
            {
                this->selectPlayerBoard(this->m_p1.m_ships[i].m_shipParts[j].getPosition()[0], this->m_p1.m_ships[i].m_shipParts[j].getPosition()[1]);
            }
            //std::cout<<"**";
        }
    }
    else
    {
        for(unsigned int i=0; i<this->m_p2.m_ships.size();i++)
        {
            for(int j=0; j<this->m_p2.m_ships[i].getLength(); j++)
            {
                this->selectPlayerBoard(this->m_p2.m_ships[i].m_shipParts[j].getPosition()[0], this->m_p2.m_ships[i].m_shipParts[j].getPosition()[1]);
            }
            //std::cout<<"**";
        }
    }



}

void Game::afficheOpponent()
{
    if(this->getTurn()==1)
    {
        for(unsigned int i=0; i<this->m_p2.m_ships.size();i++)
        {
            for(int j=0; j<this->m_p2.m_ships[i].getLength(); j++)
            {
                if(this->m_p2.m_ships[i].m_shipParts[j].getTouche())
                this->selectOpponentBoard(this->m_p2.m_ships[i].m_shipParts[j].getPosition()[0], this->m_p2.m_ships[i].m_shipParts[j].getPosition()[1]);
            }
            //std::cout<<"**";
        }
    }
    else
    {
        for(unsigned int i=0; i<this->m_p1.m_ships.size();i++)
        {
            for(int j=0; j<this->m_p1.m_ships[i].getLength(); j++)
            {
                if(this->m_p1.m_ships[i].m_shipParts[j].getTouche())
                this->selectOpponentBoard(this->m_p1.m_ships[i].m_shipParts[j].getPosition()[0], this->m_p1.m_ships[i].m_shipParts[j].getPosition()[1]);
            }
            //std::cout<<"**";
        }
    }



}

//La classique fonction gotoligcol qui permet d'acc�der � un point de l'�cran
void Game::gotoligcol( int lig, int col)
{
    COORD mycoord;
    mycoord.X = col;
    mycoord.Y = lig;
    SetConsoleCursorPosition( GetStdHandle( STD_OUTPUT_HANDLE ), mycoord );
}

/*
Permet d'acc�der � une case du tableau du joueur actuel.
Pour acc�der � la case (c;3), j'utilise la fonction comme suit:
selectPlayerBoard('c',3);
*/
void Game::selectPlayerBoard(int letter, int number)
{
    this->gotoligcol(letter*2+3,number*3+3);
    std::cout<<"**";//modifier apres que la classe ship ait �t� termin�e
}
void Game::goToPlayerBoard(int letter, int number)
{
    this->gotoligcol(letter*2+3,number*3+3);
}

/*
Permet d'acc�der � une case du tableau du joueur actuel.
Pour acc�der � la case (c;3), j'utilise la fonction comme suit:
selectOpponentBoard('c',3);
*/
void Game::selectOpponentBoard(int letter, int number)
{
    this->gotoligcol(letter*2+3,number*3+61);
    std::cout<<"ok";//modifier apres que la classe ship ait �t� termin�e
}
void Game::goToOpponentBoard(int letter, int number)
{
    this->gotoligcol(letter*2+3,number*3+61);
}

void Game::boucle()
{
    bool winCond;
    char a;
    this->curseur.setPosition(7,7);
    this->affiche();
    while(this->getWin()==0){
            winCond=true;
            for(int i=0; i<this->m_p1.m_ships.size(); i++){
                for(int j=0; j<this->m_p1.m_ships[i].m_shipParts.size();j++){
                    if(!this->m_p1.m_ships[i].m_shipParts[j].getTouche())winCond=false;
                }
            }
            if(winCond==true)setWin(1);
            winCond=true;
            for(int i=0; i<this->m_p2.m_ships.size(); i++){
                for(int j=0; j<this->m_p2.m_ships[i].m_shipParts.size();j++){
                    if(!this->m_p2.m_ships[i].m_shipParts[j].getTouche())winCond=false;
                }
            }
            if(winCond==true)setWin(2);

            if(kbhit())
            {
                this->affiche();
                a=getch();
                this->goToPlayerBoard(this->curseur.getPosition()[0],this->curseur.getPosition()[1]);
                if(a=='z' && this->curseur.getPosition()[0]>=1)this->curseur.setPosition(this->curseur.getPosition()[0]-1,this->curseur.getPosition()[1]);
                if(a=='d' && this->curseur.getPosition()[1]<=13)this->curseur.setPosition(this->curseur.getPosition()[0],this->curseur.getPosition()[1]+1);
                if(a=='s' && this->curseur.getPosition()[0]<=13)this->curseur.setPosition(this->curseur.getPosition()[0]+1,this->curseur.getPosition()[1]);
                if(a=='q' && this->curseur.getPosition()[1]>=1)this->curseur.setPosition(this->curseur.getPosition()[0],this->curseur.getPosition()[1]-1);
                if(a==' '){

                    this->selectShip();

                    if(this->getTurn()==1)this->setTurn(2);
                    else this->setTurn(1);
                }
            }
    }
}

void Game::selectShip()
{
    if(getTurn()==1){
        for(int i=0; i<this->m_p1.m_ships.size(); i++){
            for(int j=0; j<this->m_p1.m_ships[i].m_shipParts.size();j++){
                if(this->m_p1.m_ships[i].m_shipParts[j].getPosition()==this->curseur.getPosition()){

                    tir(this->m_p1.m_ships[i].getPower());

                }
            }
        }
    }
    else{
        for(int i=0; i<this->m_p2.m_ships.size(); i++){
            for(int j=0; j<this->m_p2.m_ships[i].m_shipParts.size();j++){
                if(this->m_p2.m_ships[i].m_shipParts[j].getPosition()==this->curseur.getPosition()){

                    tir(this->m_p2.m_ships[i].getPower());

                }
            }
        }
    }
}

void Game::tir(int puissance)
{
    char a;
    bool cond=false;
    this->curseur.setPosition(1,1);
     while(!cond){
            if(kbhit())
            {
                a=getch();
                if(a=='z' && this->curseur.getPosition()[0]>=1)this->curseur.setPosition(this->curseur.getPosition()[0]-1,this->curseur.getPosition()[1]);
                if(a=='d' && this->curseur.getPosition()[1]<=13)this->curseur.setPosition(this->curseur.getPosition()[0],this->curseur.getPosition()[1]+1);
                if(a=='s' && this->curseur.getPosition()[0]<=13)this->curseur.setPosition(this->curseur.getPosition()[0]+1,this->curseur.getPosition()[1]);
                if(a=='q' && this->curseur.getPosition()[1]>=1)this->curseur.setPosition(this->curseur.getPosition()[0],this->curseur.getPosition()[1]-1);
                if(a==' '){cond=universeShip();}
                this->goToOpponentBoard(this->curseur.getPosition()[0],this->curseur.getPosition()[1]);
            }
    }
}

bool Game::universeShip()
{
    if(getTurn()==1){
        for(int i=0; i<this->m_p2.m_ships.size(); i++){
            for(int j=0; j<this->m_p2.m_ships[i].m_shipParts.size();j++){
                if(this->m_p2.m_ships[i].m_shipParts[j].getPosition()==this->curseur.getPosition()){
                   this->m_p2.m_ships[i].m_shipParts[j].setTouche(true);
                   return true;
                }
            }
        }
        return true;
    }
    else{
        for(int i=0; i<this->m_p1.m_ships.size(); i++){
            for(int j=0; j<this->m_p1.m_ships[i].m_shipParts.size();j++){
                if(this->m_p1.m_ships[i].m_shipParts[j].getPosition()==this->curseur.getPosition()){
                   this->m_p1.m_ships[i].m_shipParts[j].setTouche(true);
                   return true;
                }
            }
        }
        return true;
    }
}
