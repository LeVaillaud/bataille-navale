#ifndef PLAYER_H
#define PLAYER_H
#include "SousMarin.h"
#include "Destroyer.h"
#include "Croiseur.h"
#include "Cuirasse.h"
#include <ctime>
#include <cstdlib>


class Player
{
    public:
        Player();
        ~Player();

        std::vector<Ship> m_ships;

        void setShips();
//        std::vector<Ship> getShip()
//        {
//            return m__sousMarins;
//        };
        void placeShips();

        int m_num;

    protected:

    private:
};

#endif // PLAYER_H
