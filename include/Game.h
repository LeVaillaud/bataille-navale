#ifndef GAME_H
#define GAME_H
#include "Player.h"

#define tailletab 15

class Game
{
    public:
        Game();
        virtual ~Game();

        int getWin() { return m_win; }
        void setWin(unsigned int val) { m_win = val; }
        int getTurn() { return m_turn; }
        void setTurn(unsigned int val) { m_turn = val; }

        void affiche();
        void afficheJoueur();
        void afficheOpponent();
        void gotoligcol( int lig, int col );
        void selectPlayerBoard(int letter, int number);
        void goToPlayerBoard(int,int);
        void selectOpponentBoard(int letter, int number);
        void goToOpponentBoard(int, int);
        void boucle();
        void selectShip();
        void tir(int puissance);
        bool universeShip();

        Player m_p1;
        Player m_p2;

        ShipPart curseur;

    protected:

    private:
        int m_win;
        int m_turn;
};

#endif // GAME_H
