#ifndef SHIP_H
#define SHIP_H
#include <iostream>
#include <vector>
#include "ShipPart.h"

class Ship
{
    public:
        Ship(int _length, int _power);
        virtual ~Ship();

        int getLength() {return m_length;}
        void setLength(int val) {m_length = val;}
        int getPower() {return m_power;}
        void setPower(int val) {m_power = val;}

        std::vector<ShipPart> m_shipParts;
        void setShipParts();

    protected:

    private:
        int m_length;
        int m_power;

};

#endif // SHIP_H
