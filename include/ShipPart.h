#ifndef SHIPPART_H
#define SHIPPART_H
#include <vector>

class ShipPart
{
    public:
        ShipPart();
        virtual ~ShipPart();
        bool getTouche(){return m_touche;}
        void setTouche(bool touche){m_touche=touche;}
        std::vector<int> getPosition() {return m_position;}
        void setPosition(int x, int y)
        {
            if(m_position.empty())
            {
                m_position.push_back(x);
                m_position.push_back(y);
            }
            else
            {
                m_position[0]=x;
                m_position[1]=y;
            }
        }


    protected:

    private:
        std::vector<int> m_position;
        bool m_touche;
};

#endif // SHIPPART_H
