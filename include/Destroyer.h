#ifndef DESTROYER_H
#define DESTROYER_H
#include "Ship.h"

class Destroyer : public Ship
{
    public:
        Destroyer();
        ~Destroyer();

    protected:

    private:
};

#endif // DESTROYER_H
